/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.oxgui;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Game {
    Scanner kb = new Scanner(System.in);
    private Player O;
    private Player X;
    private int row;
    private int col;
    private Board board;
    
    public Game() {
        this.O = new Player('O');
        this.X = new Player('X');
    }
    
    public void newBoard() {
        this.board = new Board(X,O);
    }
    
    public void showTurn() {
        Player player = board.getCurrentPlayer();
        System.out.println("Turn " + player.getSymbol());
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }
    
    public void showTable() {
        char [][] table = board.getTable();
        for(int row = 0; row < table.length; row++) {
            for(col = 0; col < table.length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println(" ");
        }
    }
    
    public void inputRowCol() {
        while(true) {
            System.out.println("Please input row , col : ");
            row = kb.nextInt();
            col = kb.nextInt();
            
            if(board.setRowCol(row,col)) {
                return;
            }
        }
    }
    
    public boolean isFinish() {
        if(board.isDraw() || board.isWin()){
            return true;
        }
        return false;
    }
   public void showStat() {
       System.out.println(X);
       System.out.println(O);
   }
    
    public void showResult() {
        if(board.isDraw()) {
            System.out.println("Draw !!");
        } else if (board.isWin()) {
            System.out.println(board.getCurrentPlayer().getSymbol()+ " Win");
        }
    }


        
}
    

